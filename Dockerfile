FROM openjdk:8-jdk-alpine

COPY target/helloworld-0.0.1.jar .

ENTRYPOINT ["java", "-jar", "helloworld-0.0.1.jar"]